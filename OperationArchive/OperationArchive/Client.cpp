#include <iostream>
#include "OperationArchive.h"

OperationArchive* OperationArchive::instance = nullptr;

int main()
{
    OperationArchive* archive = OperationArchive::getInstance();
    archive->addOperation(Transaction
        {
            "operation3",
            Account {"Alina", 50 },
            Account {"Alina1", 150 }
        });
}