#ifndef OPERATION_ARCHIVE_H
#define OPERATION_ARCHIVE_H

#include <string>
#include <vector>
#include <fstream>
#include <sstream>

class Account
{
public:
    Account() {}
    Account(std::string name, int amount) : _name(name), _amount(amount) {}

    std::string getName() const 
    {
        return _name;
    }
    int getAmount() const 
    {
        return _amount;
    }
private:
    std::string _name;
    int _amount = 0;
};

struct Transaction 
{
	std::string _operation_name;
	Account _sender;
	Account _receiver;
};

class OperationArchive 
{
public: 
    static OperationArchive* getInstance()
    {
        if (instance == nullptr)
            instance = new OperationArchive();
        return instance;
    }
    void addOperation(const Transaction& transaction) 
    {
        std::ofstream fout;
        fout.open(archivePath, std::ios_base::app);
        writeTransaction(fout, transaction);
        fout.close();

        operation_history.push_back(transaction);
    }

private:
    OperationArchive() 
    {
        initHistory();
    }
    void initHistory() 
    {
        std::ifstream fin(archivePath);
        std::string temp;
        while (std::getline(fin, temp))
        {
            std::stringstream ss(temp); 
            std::string operation_name;
            std::getline(ss, operation_name, ' ');
            operation_history.emplace_back(Transaction { operation_name, readAccount(ss), readAccount(ss) });
        }
        fin.close();
    }
    Account readAccount(std::stringstream& ss) 
    {
        std::string name, amount;
        std::getline(ss, name, ' ');
        std::getline(ss, amount, ' ');

        return Account { name, std::stoi(amount) };
    }
    void writeAccount(std::ofstream& fout, const Account& acc)
    {
        fout << acc.getName() << " " << acc.getAmount();
    }
    void writeTransaction(std::ofstream& fout, const Transaction& transaction)
    {
        fout << "\n" << transaction._operation_name << " ";
        writeAccount(fout, transaction._sender);
        fout << " ";
        writeAccount(fout, transaction._receiver);
    }

    const std::string archivePath = "archive.txt";
    static OperationArchive* instance;
    std::vector<Transaction> operation_history;
};

#endif // !OPERATION_ARCHIVE_H
